from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, PrimaryKeyConstraint, delete
from datetime import date
import os

if os.environ.get('DEV'):
    engine = create_engine('sqlite:///sqlalchemy_example.db', echo=True)
    Base = declarative_base()
else:
    user = os.environ["MYSQL_USER"]
    password = os.environ["MYSQL_PASSWORD"]
    host = os.environ["MYSQL_HOST"]
    db = os.environ["MYSQL_DB"]
    engine = create_engine('mysql+pymysql://%s:%s@%s/%s' % (user, password, host, db))
    Base = declarative_base()


class User(Base):
    __tablename__ = 'users'
    id = Column(Integer, primary_key=True)
    name = Column(String(255), nullable=False, unique=True)
    birthday = Column(String(20), nullable=False)
    primary = PrimaryKeyConstraint('id', name = "id_idx")
    def __repr__(self):
       return "<User(name='%s', birthday='%s')>" % (
                           self.name, self.birthday)
Base.metadata.create_all(engine)


def split_birthday(birthday):
    res = list()
    b = birthday.split("-")
    year = int(b[0])
    month = int(b[1])
    day = int(b[2])
    res.append(year)
    res.append(month)
    res.append(day)
    return res

def Birthday(birthday):
    today = date.today()
    this_year = today.year
    splitbirthday = split_birthday(birthday)
    month = splitbirthday[1]
    day = splitbirthday[2]
    my_birthday = date(this_year, month, day)
    delta = my_birthday - today
    if int(str(delta.days)) < 0:
        return 365 + int(str(delta.days))
    elif int(str(delta.days)) == 0:
        return 0
    else:
        return int(str(delta.days))

def compare_birthday_with_today(birthday):
    today = date.today()
    splitbirthday = split_birthday(birthday)
    year = splitbirthday[0]
    month = splitbirthday[1]
    day = splitbirthday[2]
    my_birthday = date(year, month, day)
    if my_birthday == today:
        return 1
    else:
        return 0
