import tornado.ioloop
import tornado.web
import json
from sqlalchemy.orm import sessionmaker
from functions import *


class MainHandler(tornado.web.RequestHandler):
    def get(self):
        self.write("Hello, world!")


class helloHandler(tornado.web.RequestHandler):
    def prepare(self):
        self.DBSession = sessionmaker(bind=engine)
        if self.request.headers.get("Content-Type", "").startswith("application/json"):
            try:
                self.json_args = json.loads(self.request.body)
            except:
                self.json_args = None
        else:
            self.json_args = None
        try:
            self.tech = int(self.get_argument(name='tech'))
        except:
            self.tech = 0


    def get(self, user):
        session = self.DBSession()
        try:
            birthday = session.query(User).filter(User.name == user).one()
            response = dict()
            delta = Birthday(birthday.birthday)
            if self.tech != 1:
                if delta == 0:
                    response['message'] = 'Hello, %s! Happy birthday!' % user
                else:
                    response['message'] = 'Hello, %s! Your birthday is in %d days!' % (user, delta)
            else:
                print "this is technical request"
                response['message'] = delta
            self.write("%s" % json.dumps(response))
        except:
            self.set_status(404)
            self.write("Wrong requested user")


    def put(self, *args, **kwargs):
        if isinstance(self.json_args, dict):
            session = self.DBSession()
            birthday = self.json_args['dateOfBirth']
            check_wrong_data = compare_birthday_with_today(birthday)
            if check_wrong_data == 0:
                new_user = User(name='%s' % args, birthday='%s' % birthday)
                session.add(new_user)
                try:
                    session.commit()
                    self.set_status(204)
                except:
                    self.set_status(500)
                    self.write("Duplicate user name\n\r")
            else:
                self.set_status(500)
                self.write("Wrong birthday date, it can't be today\n\r")
        else:
            self.set_status(500)
            self.write("Wrong input data\n\r")

    def delete(self, *args, **kwargs):
        if isinstance(self.json_args, dict):
            session = self.DBSession()
            d = session.query(User).filter_by(name = '%s' % args[0]).one()
            session.delete(d)
            try:
                session.commit()
                self.set_status(204)
            except:
                self.set_status(500)
                self.write("Can't delete user or user not found\n\r")
        else:
            self.set_status(500)
            self.write("Wrong input data\n\r")

def make_app():
    application = tornado.web.Application(
        [
            (r"/test", MainHandler),
            (r"/hello/([a-zA-Z]+)", helloHandler)
        ],debug=True
    )
    return application


if __name__ == "__main__":
    try:
        listen_port = int(os.environ["LISTEN_PORT"])
    except:
        listen_port = 8080
    app = make_app()
    app.listen(listen_port)
    print "start birthday server...."
    tornado.ioloop.IOLoop.current().start()
